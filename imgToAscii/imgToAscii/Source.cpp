#include<opencv2/opencv.hpp>
#include<iostream>
#include <fstream>
#include <cmath>
#include <cfenv>
#include <climits>

#define SCALE_NUMBER 3.695652174

void writeToFile(int pixel, std::ofstream& file, int blackWhite);

int main()
{
    bool blackWhite = 0;
    std::string srcFile = "";
    std::cout << "enter image to convert: " << std::endl;
    std::cin >> srcFile;
    std::string dstFile = "";
    std::cout << "enter output file path: " << std::endl;
    std::cin >> dstFile;
    int jmp = 1;
    //accuracy level
    std::cout << "chooce the size(in pixels) that an ascii will represent: " << std::endl;
    std::cin >> jmp;
    std::cout << "chooce the black/white(0/else): " << std::endl;
    std::cin >> blackWhite;
    int curr = 0;
    cv::Mat img = cv::imread(srcFile, cv::IMREAD_GRAYSCALE);
    //writes a character that represent every pixel or less in the img
    if(img.data)
	{
        std::ofstream outfile(dstFile);
        if (!outfile.is_open())
        {
            std::cout << "uneble to open out file" << std::endl;
            exit(0);
        }
        for (int i = 0; i < img.rows; i += jmp)
        {
            for (int j = 0; j < img.cols; j += jmp)
            {
                try
                {
                    curr = int(img.at<uchar>(i, j));
                    writeToFile(curr, outfile, blackWhite);
                }
                catch (...) { exit(1); };
            }
            outfile << std::endl;
        }
    }
    else
    {
        std::cout << "uneble to open img" << std::endl;
    }
    system("pause");
	return 0;
}

/*
this func gets a pixel, calc it's britness level (on a scale 0-50) and appends the right character to the txt file
input: output file, order of levels(black to white or white to black), the pixel value in cv:mat
output: none
*/
void writeToFile(int pixel, std::ofstream& file, int blackWhite)
{
    std::string britness = "$@B%8&WM#*CJUYXzcnxrt/|()1{}[]?-_+~<>i!lI;:,\" ^ `'. ";
    if (blackWhite)
    {
        britness = " .'` ^ \",:;Il!i><~+_-?][}{1)(|/trxnczXYUJC*#MW&8%B@$";
    }
    int level = std::round(pixel / (double)SCALE_NUMBER);
    if (level < 0 || level > 50)
    {
        level = 0;
    }
    file << britness[level] << "  ";
}
